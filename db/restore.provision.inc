<?php

function drush_db_provision_restore_validate() {
  d()->service('db')->connect();
}

function drush_db_post_provision_restore() {
  if (d()->site_status != -2) {
    d()->service('db')->destroy_site_database();
  }
}

